package com.aait.taketable.Models

import java.io.Serializable

class ReservationModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var distance:Float?=null
    var date:String?=null
    var time:String?=null
    var status:String?=null
    var numbers:Int?=null
    var desc:String?=null
    var food_type:String?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
    var rate:Float?=null
}