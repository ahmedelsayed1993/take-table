package com.aait.taketable.Models

import java.io.Serializable

class ProviderModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var desc:String?=null
    var food_type:String?=null
    var icon:String?=null
    var rate:Float?=null
    var rate_price:Float?=null
    var user_id:Int?=null
    var distance:Float?=null
    var is_open:Boolean?=null
    var lat:String?=null
    var lng:String?=null
    var discount:String?=null
}