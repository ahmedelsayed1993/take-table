package com.aait.taketable.Models

import java.io.Serializable

class CategoriesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var icon:String?=null
    var allow_reservation:Boolean?=null
    var reservation_type:String?=null
    var show_subcategory:Boolean?=null
    var providers:ArrayList<ProviderModel>?=null

}