package com.aait.taketable.Models

import java.io.Serializable

class FavouritesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var desc:String?=null
    var food_type:String?=null
    var distance:Float?=null
    var fav:Int?=null
    var address:String?=null
    var lat:String?=null
    var lng:String?=null
}