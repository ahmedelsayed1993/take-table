package com.aait.taketable.Models

import java.io.Serializable

class HomeModel:Serializable {
    var slider:ArrayList<String>?=null
    var category:ArrayList<CategoriesModel>?=null
    var offers:ArrayList<ProviderModel>?=null
    var providers:ArrayList<ProviderModel>?=null
}