package com.aait.taketable.Models

import java.io.Serializable

class ProductsResponse:BaseResponse(),Serializable {
    var data:ArrayList<ProductModel>?=null
}