package com.aait.taketable.Models

import java.io.Serializable

class ProviderDetailsModel:Serializable {
    var images:ArrayList<ImageModel>?=null
    var id:Int?=null
    var name:String?=null
    var category:String?=null
    var category_id:Int?=null
    var allow_reservation:String?=null
    var reservation_type:String?=null
    var show_subcategory:String?=null
    var distance:String?=null
    var rate:Float?=null
    var rate_price:Float?=null
    var fav:Int?=null
    var phone:String?=null
    var desc:String?=null
    var food_type:String?=null
    var feature:String?=null
    var address:String?=null
    var capacity:Int?=null
    var lat:String?=null
    var lng:String?=null
    var start:String?=null
    var end:String?=null
    var email:String?=null
    var link_provider:String?=null
    var review:Int?=null
    var is_open:Boolean?=null
    var reservation_price:String?=null
    var birthday:String?=null
    var marriage_anniversary:String?=null
    var business_meeting:String?=null
    var special_event:String?=null
    var outdoor:String?=null
    var indoor:String?=null
    var surface_view:String?=null
    var music:String?=null
    var kids:String?=null
    var smoking:String?=null
    var motivational_owners:String?=null
}