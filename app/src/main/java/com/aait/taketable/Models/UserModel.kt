package com.aait.taketable.Models

import java.io.Serializable

class UserModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var phone:String?=null
    var phonekey:String?=null
    var email:String?=null
    var code:String?=null
    var device_id:String?=null
   // var device_type:String?=null
    var avatar:String?=null
    var active:String?=null
   // var driver:Int?=null
    var type:String?=null

}