package com.aait.taketable.Models

import java.io.Serializable

class FavouritesResponse:BaseResponse(),Serializable {
    var data:ArrayList<FavouritesModel>?=null
}