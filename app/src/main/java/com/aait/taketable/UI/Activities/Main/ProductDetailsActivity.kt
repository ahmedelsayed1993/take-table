package com.aait.taketable.UI.Activities.Main

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.GPS.GPSTracker
import com.aait.taketable.GPS.GpsTrakerListener
import com.aait.taketable.Models.BaseResponse
import com.aait.taketable.Models.ImageModel
import com.aait.taketable.Models.ProviderDetailsResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Auth.LoginActivity
import com.aait.taketable.UI.Controllers.SliderAdapter
import com.aait.taketable.UI.Controllers.SlidersAdapter
import com.aait.taketable.Utils.CommonUtil
import com.aait.taketable.Utils.DialogUtil
import com.aait.taketable.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class ProductDetailsActivity:ParentActivity(), OnMapReadyCallback,
    GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_product_details
    lateinit var back:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var images:TextView
    lateinit var name:TextView
    lateinit var rating:RatingBar
    lateinit var type:TextView
    lateinit var distance:TextView
    lateinit var description:TextView
    lateinit var fav:ImageView
    lateinit var share:ImageView
    lateinit var phone:ImageView
    lateinit var products:ImageView
    lateinit var map:MapView
    lateinit var go_now:Button
    lateinit var rate:Button
    lateinit var additional:TextView
    lateinit var book:Button
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal  var myMarker: Marker?=null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var id = 0
    var link = ""
    var num = ""
    var Lat=""
    var Lng = ""
    var imagesModels = ArrayList<ImageModel>()
    var features = ArrayList<String>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        viewPager = findViewById(R.id.viewPager)
        images = findViewById(R.id.images)
        name = findViewById(R.id.name)
        rating = findViewById(R.id.rating)
        type = findViewById(R.id.type)
        distance = findViewById(R.id.distance)
        description = findViewById(R.id.description)
        fav = findViewById(R.id.fav)
        share = findViewById(R.id.share)
        phone = findViewById(R.id.phone)
        products = findViewById(R.id.products)
        map = findViewById(R.id.map)
        go_now = findViewById(R.id.go_now)
        rate = findViewById(R.id.rate)
        additional = findViewById(R.id.additional)
        book = findViewById(R.id.book)
        products.setOnClickListener { val intent = Intent(this,ProductsActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)}
        fav.setOnClickListener { if (user.loginStatus!!){PostFav() }
        else{
            startActivity(Intent(this, LoginActivity::class.java))
        }}
        book.setOnClickListener {
            if (user.loginStatus!!) {
                val intent = Intent(this, DateTimeActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        getLocationWithPermission()
        share.setOnClickListener { CommonUtil.ShareApp(mContext) }

        phone.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_call)

            val number = dialog?.findViewById<TextView>(R.id.number)
            val cancel = dialog?.findViewById<TextView>(R.id.cancel)
            val confirm = dialog?.findViewById<TextView>(R.id.confirm)
            number.text = num
            cancel.setOnClickListener { dialog?.dismiss() }
            confirm.setOnClickListener { getLocationWithPermission(num)
            dialog?.dismiss()}
            dialog?.show()
        }

        rate.setOnClickListener {
            if (user.loginStatus!!) {
                val dialog = Dialog(mContext)
                dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog?.window!!.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                dialog?.setCancelable(true)
                dialog?.setContentView(R.layout.dialog_rate)

                val rating = dialog?.findViewById<RatingBar>(R.id.rating)
                val rating_price = dialog?.findViewById<RatingBar>(R.id.rating_price)
                val comment = dialog?.findViewById<EditText>(R.id.comment)
                val confirm = dialog?.findViewById<Button>(R.id.confirm)


                confirm.setOnClickListener {
                    if (CommonUtil.checkEditError(comment, getString(R.string.add_comment))) {
                        return@setOnClickListener
                    } else {
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.Rate(
                            lang.appLanguage,
                            user.userData.id!!,
                            id,
                            rating.rating.toInt(),
                            rating_price.rating.toInt(),
                            comment.text.toString()
                        )
                            ?.enqueue(object : Callback<BaseResponse> {
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext, t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                    dialog?.dismiss()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    dialog?.dismiss()
                                    if (response.isSuccessful) {
                                        if (response.body()?.value.equals("1")) {
                                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                        } else {
                                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                        }
                                    }
                                }

                            })
                    }
                }
                dialog?.show()
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

        go_now.setOnClickListener { startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang + "&daddr=" + Lat + "," + Lng)
            )) }

        images.setOnClickListener { if (imagesModels.size==0){
        }else{
            val intent = Intent(this,ImagesActivity::class.java)
            intent.putExtra("images",imagesModels)
            startActivity(intent)
        }
        }

    }

    fun getData(user_id:Int?,lat:String,lng:String){
       // showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProvider(lang.appLanguage,user_id,id,lat,lng)?.enqueue(object :
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.body()?.value.equals("1")){
                    initSliderAds(response.body()?.data?.images!!)
                    imagesModels.clear()
                    for (i in 0..response.body()?.data?.images!!.size-1){
                        imagesModels.add(response.body()?.data?.images!!.get(i))
                    }
                    images.text = response.body()?.data?.images?.size.toString()+ " "+getString(R.string.show_all_photos)
                    name.text = response.body()?.data?.name
                    rating.rating = response.body()?.data?.rate!!
                    type.text = response.body()?.data?.food_type
                   // link = response.body()?.data?.link_provider!!
                    distance.text = getString(R.string.Away_from_you)+response.body()?.data?.distance+getString(R.string.km)
                    num = response.body()?.data?.phone!!
                    Lat = response.body()?.data?.lat!!
                    Lng = response.body()?.data?.lng!!
                    if (response.body()?.data?.fav==0){
                        fav.setImageResource(R.mipmap.addfavouriteicon)
                    }else{
                        fav.setImageResource(R.mipmap.favouriteicon)
                    }
                    if (response.body()?.data?.allow_reservation.equals("true")){
                        book.visibility = View.VISIBLE
                    }else{
                        book.visibility = View.GONE
                    }
                    description.text = response.body()?.data?.desc
                    putMapMarker(response.body()?.data?.lat!!.toDouble(),response.body()?.data?.lng!!.toDouble())
                    if (response.body()?.data?.birthday.equals("true")){
                        features.add(getString(R.string.Birth_anniversary))
                    }
                    if (response.body()?.data?.business_meeting.equals("true")){
                        features.add(getString(R.string.business_meeting))
                    }
                    if (response.body()?.data?.indoor.equals("true")){
                        features.add(getString(R.string.in_door))
                    }
                    if (response.body()?.data?.kids.equals("true")){
                        features.add(getString(R.string.Allow_children))
                    }
                    if (response.body()?.data?.marriage_anniversary.equals("true")){
                        features.add(getString(R.string.marriage_anniversary))
                    }
                    if (response.body()?.data?.motivational_owners.equals("true")){
                        features.add(getString(R.string.Suitable_for_people_of_determination))
                    }
                    if (response.body()?.data?.music.equals("true")){
                        features.add(getString(R.string.Availability_of_music))
                    }
                    if (response.body()?.data?.outdoor.equals("true")){
                        features.add(getString(R.string.out_door))
                    }
                    if (response.body()?.data?.smoking.equals("true")){
                        features.add(getString(R.string.Allow_smoking))
                    }
                    if (response.body()?.data?.special_event.equals("true")){
                        features.add(getString(R.string.special_event))
                    }
                    if (response.body()?.data?.surface_view.equals("true")){
                        features.add(getString(R.string.Roof_view))
                    }
                    Log.e("fea",features.joinToString(separator = ",",postfix = "",prefix = ""))
                     additional.text =  features.joinToString(separator = ",",postfix = "",prefix = "")

                }
            }

        })
    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun PostFav(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddFav(lang.appLanguage,user.userData.id!!,id)?.enqueue(object :Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        getData(user.userData.id,gps.latitude.toString(),gps.longitude.toString())
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
        getLocationWithPermission()
        hideProgressDialog()
    }
        fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap?.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.outline_location_on))
        )!!
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun initSliderAds(list: java.util.ArrayList<ImageModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            viewPager.adapter= SlidersAdapter(mContext!!,list)
        }
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (user.loginStatus!!) {
                    getData(user.userData.id,gps.latitude.toString(), gps.longitude.toString())
                }else{
                    getData(null,gps.latitude.toString(), gps.longitude.toString())
                }

            }
        }
    }
}