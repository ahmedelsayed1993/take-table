package com.aait.taketable.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.taketable.Base.ParentRecyclerAdapter
import com.aait.taketable.Base.ParentRecyclerViewHolder
import com.aait.taketable.Models.FavouritesModel
import com.aait.taketable.Models.ProviderModel
import com.aait.taketable.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class FavouritesAdapter (context: Context, data: MutableList<FavouritesModel>, layoutId: Int) :
    ParentRecyclerAdapter<FavouritesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)



    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        viewHolder.type.text = questionModel.food_type
        viewHolder.distance.text = mcontext.getString(R.string.Away_from_you)+questionModel.distance.toString()+mcontext.getString(
            R.string.km)
        viewHolder.address.text = questionModel.address
        viewHolder.rating.visibility = View.GONE


        // viewHolder.itemView.animation = mcontext.resources.
        // val animation = mcontext.resources.getAnimation(R.anim.item_animation_from_right)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_right)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })

        viewHolder.heart.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<CircleImageView>(R.id.image)
        internal var type = itemView.findViewById<TextView>(R.id.type)
        internal var distance = itemView.findViewById<TextView>(R.id.distance)
        internal var address = itemView.findViewById<TextView>(R.id.address)
        internal var heart = itemView.findViewById<ImageView>(R.id.heart)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)



    }
}