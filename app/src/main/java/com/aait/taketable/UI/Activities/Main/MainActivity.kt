package com.aait.taketable.UI.Activities.Main

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Auth.LoginActivity
import com.aait.taketable.UI.Fragments.FavouriteFragment
import com.aait.taketable.UI.Fragments.HomeFragment
import com.aait.taketable.UI.Fragments.MoreFragment
import com.aait.taketable.UI.Fragments.ReservationFragment

class MainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var home: LinearLayout
    lateinit var home_image: ImageView
    lateinit var home_text: TextView
    lateinit var back:ImageView
    lateinit var favourite: LinearLayout
    lateinit var favourite_image: ImageView
    lateinit var favourite_text: TextView

    lateinit var my_reservations: LinearLayout

    lateinit var reservation_image: ImageView
    lateinit var reservation_text: TextView
    lateinit var more: LinearLayout

    lateinit var more_image: ImageView
    lateinit var more_text: TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var favouriteFragment: FavouriteFragment
    internal lateinit var reservationFragment: ReservationFragment
    internal lateinit var moreFragment: MoreFragment
    lateinit var title: TextView
    lateinit var notification: ImageView



    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        my_reservations = findViewById(R.id.my_reservations)
        reservation_image = findViewById(R.id.reservation_image)
        reservation_text = findViewById(R.id.reservation_text)
        favourite = findViewById(R.id.favourite)
        favourite_image = findViewById(R.id.favourite_image)
        favourite_text = findViewById(R.id.favourite_text)
        more = findViewById(R.id.more)
        more_image = findViewById(R.id.more_image)
        more_text = findViewById(R.id.more_text)

        back.setOnClickListener { startActivity(Intent(this,MapActivity::class.java)) }
        notification = findViewById(R.id.notification)
        if (user.loginStatus!!){
            notification.visibility = View.VISIBLE
        }else{
            notification.visibility = View.GONE
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java))
        }
        homeFragment = HomeFragment.newInstance()
        reservationFragment = ReservationFragment.newInstance()
        favouriteFragment = FavouriteFragment.newInstance()
        moreFragment = MoreFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, favouriteFragment)
        transaction!!.add(R.id.home_fragment_container,reservationFragment)
        transaction!!.add(R.id.home_fragment_container,moreFragment)
        transaction!!.commit()

        showhome()
        home.setOnClickListener { showhome() }
        favourite.setOnClickListener {
            if (user.loginStatus!!){
                showreservation()
            } else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        my_reservations.setOnClickListener {
            if (user.loginStatus!!){
                showpurchase()
            }else{
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        more.setOnClickListener { showmore() }

    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showhome(){
        selected = 1
        back.visibility = View.VISIBLE
        home_image.setImageResource(R.mipmap.homeactive)
        home_text.textColor = Color.parseColor("#ECB832")
        reservation_text.textColor = Color.parseColor("#C1C5C9")
        favourite_text.textColor = Color.parseColor("#C1C5C9")
        more_text.textColor = Color.parseColor("#C1C5C9")
        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.addedfavouriteicon)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showreservation(){
        selected = 1
        back.visibility = View.GONE
        home_image.setImageResource(R.mipmap.home)
        favourite_text.textColor = Color.parseColor("#ECB832")
        home_text.textColor = Color.parseColor("#C1C5C9")
        reservation_text.textColor = Color.parseColor("#C1C5C9")
        more_text.textColor = Color.parseColor("#C1C5C9")

        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.favactive)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, favouriteFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showpurchase(){
        selected = 1
        back.visibility = View.GONE
        home_image.setImageResource(R.mipmap.home)
        reservation_text.textColor = Color.parseColor("#ECB832")
        favourite_text.textColor = Color.parseColor("#C1C5C9")
        home_text.textColor = Color.parseColor("#C1C5C9")
        more_text.textColor = Color.parseColor("#C1C5C9")

        reservation_image.setImageResource(R.mipmap.orderactive)
        favourite_image.setImageResource(R.mipmap.addedfavouriteicon)
        more_image.setImageResource(R.mipmap.more)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, reservationFragment)
        transaction!!.commit()
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showmore(){
        selected = 1
        back.visibility = View.GONE
        home_image.setImageResource(R.mipmap.home)
        more_text.textColor = Color.parseColor("#ECB832")
        reservation_text.textColor = Color.parseColor("#C1C5C9")
        favourite_text.textColor = Color.parseColor("#C1C5C9")
        home_text.textColor = Color.parseColor("#C1C5C9")
        reservation_image.setImageResource(R.mipmap.order)
        favourite_image.setImageResource(R.mipmap.addedfavouriteicon)
        more_image.setImageResource(R.mipmap.moreactive)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, moreFragment)
        transaction!!.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}