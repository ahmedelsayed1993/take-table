package com.aait.taketable.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.GPS.GPSTracker
import com.aait.taketable.GPS.GpsTrakerListener
import com.aait.taketable.Listeners.OnItemClickListener
import com.aait.taketable.Models.FilterModel
import com.aait.taketable.Models.FilterResponse
import com.aait.taketable.Models.ProviderDetailsResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.FilterAdapter
import com.aait.taketable.Utils.CommonUtil
import com.aait.taketable.Utils.DialogUtil
import com.aait.taketable.Utils.PermissionUtils
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class SearchActivity:ParentActivity()  , OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_search
    lateinit var filter:ImageView
    lateinit var lay:LinearLayout
    lateinit var cancel:ImageView
    lateinit var available:CheckBox
    lateinit var unavailable:CheckBox
    lateinit var birthday:RadioButton
    lateinit var marriage:RadioButton
    lateinit var business:RadioButton
    lateinit var event:RadioButton
    lateinit var cheap:RadioButton
    lateinit var avarage:RadioButton
    lateinit var expensive:RadioButton
    lateinit var outdoor:RadioButton
    lateinit var indoor:RadioButton
    lateinit var roof:RadioButton
    lateinit var top:RadioButton
    lateinit var low:RadioButton
    lateinit var music:CheckBox
    lateinit var children:CheckBox
    lateinit var smoking:CheckBox
    lateinit var people:CheckBox
    lateinit var filter_btn:Button
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var search:EditText
    lateinit var sear:ImageView
    var sort :String?=null
    var ocassion :String?=null
    var prices :String?=null
    var session :String?=null
    var rate:String?=null
    var more = ArrayList<String>()
    var cat = 0
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var filterAdapter:FilterAdapter
    var providers = ArrayList<FilterModel>()
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    override fun initializeComponents() {
        cat = intent.getIntExtra("cat",0)
        filter = findViewById(R.id.filter)
        lay = findViewById(R.id.lay)
        cancel = findViewById(R.id.close)
        available = findViewById(R.id.available)
        unavailable = findViewById(R.id.unavailable)
        birthday = findViewById(R.id.birthday)
        marriage = findViewById(R.id.marriage)
        business = findViewById(R.id.business)
        event = findViewById(R.id.event)
        cheap = findViewById(R.id.cheap)
        avarage = findViewById(R.id.avarage)
        expensive = findViewById(R.id.expensive)
        outdoor = findViewById(R.id.outdoor)
        indoor = findViewById(R.id.indoor)
        roof = findViewById(R.id.roof)
        top = findViewById(R.id.top)
        low = findViewById(R.id.low)

        search = findViewById(R.id.search)
        sear = findViewById(R.id.sear)
        music = findViewById(R.id.music)
        children = findViewById(R.id.children)
        smoking = findViewById(R.id.smoking)
        people = findViewById(R.id.people)
        filter_btn = findViewById(R.id.filter_btn)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        filterAdapter = FilterAdapter(mContext,providers,R.layout.recycle_filter)
        filterAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = filterAdapter
        lay.visibility = View.GONE
        filter.setOnClickListener { lay.visibility = View.VISIBLE }
        cancel.setOnClickListener { lay.visibility = View.GONE }
        available.setOnClickListener {
//            if (available.isChecked){
//
//
//                sort = ""
//            }else{
                available.isChecked = true
                unavailable.isChecked = false
                sort = "1"
            //}
        }
        unavailable.setOnClickListener {
//            if (unavailable.isChecked){
//
//                unavailable.isChecked = false
//                sort = ""
//            }else{
                available.isChecked = false
                unavailable.isChecked = true
                sort = "0"
           // }
        }
        birthday.setOnClickListener {
//            if (birthday.isChecked){
//                birthday.isChecked = false
//                marriage.isChecked = false
//                business.isChecked = false
//                event.isChecked = false
//                ocassion = ""
//            }else{
                birthday.isChecked = true
                marriage.isChecked = false
                business.isChecked = false
                event.isChecked = false
                ocassion = "birthday"
           // }
        }
        marriage.setOnClickListener {
//            if (marriage.isChecked){
//                birthday.isChecked = false
//                marriage.isChecked = false
//                business.isChecked = false
//                event.isChecked = false
//                ocassion = ""
//            }else{
                birthday.isChecked = false
                marriage.isChecked = true
                business.isChecked = false
                event.isChecked = false
                ocassion = "marriage_anniversary"
            //}
        }
        business.setOnClickListener {
//            if (business.isChecked){
//                birthday.isChecked = false
//                marriage.isChecked = false
//                business.isChecked = false
//                event.isChecked = false
//                ocassion = ""
//            }else{
                birthday.isChecked = false
                marriage.isChecked = false
                business.isChecked = true
                event.isChecked = false
                ocassion = "business_meeting"
            //}
        }
        event.setOnClickListener {
//            if (birthday.isChecked){
//                birthday.isChecked = false
//                marriage.isChecked = false
//                business.isChecked = false
//                event.isChecked = false
//                ocassion = ""
//            }else{
                birthday.isChecked = false
                marriage.isChecked = false
                business.isChecked = false
                event.isChecked = true
                ocassion = "special_event"
           // }
        }

        cheap.setOnClickListener {
//            if (cheap.isChecked){
//                cheap.isChecked = false
//                avarage.isChecked = false
//                expensive.isChecked = false
//                prices = ""
//            }else{
                cheap.isChecked = true
                avarage.isChecked = false
                expensive.isChecked = false
                prices = "low"
            //}
        }
        avarage.setOnClickListener {
//            if (avarage.isChecked){
//                cheap.isChecked = false
//                avarage.isChecked = false
//                expensive.isChecked = false
//                prices = ""
//            }else{
                cheap.isChecked = false
                avarage.isChecked = true
                expensive.isChecked = false
                prices = "average"
          //  }
        }
        expensive.setOnClickListener {
//            if (expensive.isChecked){
//                cheap.isChecked = false
//                avarage.isChecked = false
//                expensive.isChecked = false
//                prices = ""
//            }else{
                cheap.isChecked = false
                avarage.isChecked = false
                expensive.isChecked = true
                prices = "high"
          //  }
        }
        outdoor.setOnClickListener {
//            if (outdoor.isChecked){
//                outdoor.isChecked = false
//                indoor.isChecked = false
//                roof.isChecked = false
//                session = ""
//            }else{
                outdoor.isChecked = true
                indoor.isChecked = false
                roof.isChecked = false
                session = "outdoor"
           // }
        }
        indoor.setOnClickListener {
//            if (indoor.isChecked){
//                outdoor.isChecked = false
//                indoor.isChecked = false
//                roof.isChecked = false
//                session = ""
//            }else{
                outdoor.isChecked = false
                indoor.isChecked = true
                roof.isChecked = false
                session = "indoor"
           // }
        }
        roof.setOnClickListener {
//            if (roof.isChecked){
//                outdoor.isChecked = false
//                indoor.isChecked = false
//                roof.isChecked = false
//                session = ""
//            }else{
                outdoor.isChecked = false
                indoor.isChecked = false
                roof.isChecked = true
                session = "surface_view"
            //}
        }
        top.setOnClickListener {
//            if (top.isChecked){
//                top.isChecked = false
//                low.isChecked = false
//                rate = ""
//            }else{
                top.isChecked = true
                low.isChecked = false
                rate = "high"
           // }
        }
        low.setOnClickListener {
//            if (low.isChecked){
//                top.isChecked = false
//                low.isChecked = false
//                rate = ""
//            }else{
                top.isChecked = false
                low.isChecked = true
                rate = "low"
            //}
        }
        music.setOnClickListener {
//            if (music.isChecked){
//                music.isChecked = false
//                if (more.isEmpty()){
//
//                }else{
//                    if (more.contains("music")){
//                        more.remove("music")
//                    }else{
//
//                    }
//                }
//            }else{
//                music.isChecked = true
                if (!more.contains("music")){

                        more.add("music")


                }
           // }
        }
        children.setOnClickListener {
//            if (children.isChecked){
//                children.isChecked = false
//                if (more.isEmpty()){
//
//                }else{
//                    if (more.contains("kids")){
//                        more.remove("kids")
//                    }else{
//
//                    }
//                }
//            }else{

                if (!more.contains("kids")){
                    more.add("kids")
                }
          //  }
        }
        smoking.setOnClickListener {
//            if (smoking.isChecked){
//                smoking.isChecked = false
//                if (more.isEmpty()){
//
//                }else{
//                    if (more.contains("smoking")){
//                        more.remove("smoking")
//                    }else{
//
//                    }
//                }
//            }else{

                if (!more.contains("smoking")){
                    more.add("smoking")
                }
           // }
        }
        people.setOnClickListener {
//            if (people.isChecked){
//                people.isChecked = false
//                if (more.isEmpty()){
//
//                }else{
//                    if (more.contains("motivational_owners")){
//                        more.remove("motivational_owners")
//                    }else{
//
//                    }
//                }
//            }else{

                if (!more.contains("motivational_owners")){
                    more.add("motivational_owners")
                }
            //}
        }

        filter_btn.setOnClickListener {
//            Log.e("sort",sort!!)
//            Log.e("ocassion",ocassion!!)
//            Log.e("prices" ,prices!!)
//            Log.e("session",session!!)
//            Log.e("rate",rate!!)
            Log.e("more",Gson().toJson(more))
            lay.visibility = View.GONE
            getLocationWithPermission()
        }
        sear.setOnClickListener { getLocationWithPermission() }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission()

        }
        getLocationWithPermission()
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProductDetailsActivity::class.java)
        intent.putExtra("id",providers.get(position).id)
        startActivity(intent)
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(mContext, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                if (more.isEmpty()){
                    getData(gps.latitude.toString(),gps.longitude.toString(),null,null)
                }else{
                    getData(gps.latitude.toString(),gps.longitude.toString(),Gson().toJson(more),null)
                }
               // getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
    fun getData(lat:String,lng:String,features:String?,text:String?){
        // showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        if (cat==0) {
            Client.getClient()?.create(Service::class.java)?.Search(
                lang.appLanguage,
                null,
                lat,
                lng,
                search.text.toString(),
                prices,
                rate,
                sort,
                ocassion,
                session,
                features
            )?.enqueue(object :
                Callback<FilterResponse> {
                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    more.clear()
                    if (response.body()?.value.equals("1")) {
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            filterAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }

            })
        }else{
            Client.getClient()?.create(Service::class.java)?.Search(
                lang.appLanguage,
                cat,
                lat,
                lng,
                search.text.toString(),
                prices,
                rate,
                sort,
                ocassion,
                session,
                features
            )?.enqueue(object :
                Callback<FilterResponse> {
                override fun onFailure(call: Call<FilterResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<FilterResponse>,
                    response: Response<FilterResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    more.clear()
                    if (response.body()?.value.equals("1")) {
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            filterAdapter.updateAll(response.body()?.data!!)
                        }

                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }

            })
        }
    }
}