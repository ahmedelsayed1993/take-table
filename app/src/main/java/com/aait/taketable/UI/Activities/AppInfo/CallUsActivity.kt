package com.aait.taketable.UI.Activities.AppInfo

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Listeners.OnItemClickListener
import com.aait.taketable.Models.CallUsResponse
import com.aait.taketable.Models.SocialModel
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Main.MainActivity
import com.aait.taketable.UI.Controllers.SocialAdapter
import com.aait.taketable.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallUsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_call_us
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var email: TextView
    lateinit var phone: TextView
    lateinit var whats: TextView

   lateinit var social:RecyclerView
    lateinit var socialAdapter: SocialAdapter
    var socialModels = ArrayList<SocialModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        whats = findViewById(R.id.whats)

        social = findViewById(R.id.social)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        socialAdapter = SocialAdapter(mContext,socialModels,R.layout.recycle_social)
        socialAdapter.setOnItemClickListener(this)
        social.layoutManager = linearLayoutManager
        social.adapter = socialAdapter
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish() }
        title.text = getString(R.string.call_us)
        getData()


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CallUs(lang.appLanguage)?.enqueue(object:
            Callback<CallUsResponse> {
            override fun onFailure(call: Call<CallUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CallUsResponse>, response: Response<CallUsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        email.text = response.body()?.data?.email!!
                        phone.text = response.body()?.data?.phone!!
                        whats.text = response.body()?.data?.whatsapp!!
                        socialAdapter.updateAll(response.body()?.data?.socials!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (socialModels.get(position).link!!.startsWith("http"))
        {
            Log.e("here", "333")
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(socialModels.get(position).link!!)
            startActivity(i)

        } else {
            Log.e("here", "4444")
            val url = "https://"+socialModels.get(position).link!!
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

}