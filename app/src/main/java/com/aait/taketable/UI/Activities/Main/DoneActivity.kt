package com.aait.taketable.UI.Activities.Main

import android.content.Intent
import android.widget.TextView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.R

class DoneActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_done
lateinit var back:TextView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}