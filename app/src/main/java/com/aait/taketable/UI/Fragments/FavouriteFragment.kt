package com.aait.taketable.UI.Fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.FrameMetrics
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.taketable.Base.BaseFragment
import com.aait.taketable.GPS.GPSTracker
import com.aait.taketable.GPS.GpsTrakerListener
import com.aait.taketable.Listeners.OnItemClickListener
import com.aait.taketable.Models.BaseResponse
import com.aait.taketable.Models.FavouritesModel
import com.aait.taketable.Models.FavouritesResponse
import com.aait.taketable.Models.ProductsResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Main.ProductDetailsActivity
import com.aait.taketable.UI.Controllers.FavouritesAdapter
import com.aait.taketable.Utils.CommonUtil
import com.aait.taketable.Utils.DialogUtil
import com.aait.taketable.Utils.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class FavouriteFragment:BaseFragment() , OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_favourite
    companion object {
        fun newInstance(): FavouriteFragment {
            val args = Bundle()
            val fragment = FavouriteFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var favouritesAdapter: FavouritesAdapter
    var favouritesModels = ArrayList<FavouritesModel>()
    override fun initializeComponents(view: View) {
        rv_recycle =view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.VERTICAL,false)
        favouritesAdapter = FavouritesAdapter(mContext!!,favouritesModels,R.layout.recycle_favourite)
        favouritesAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = favouritesAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getLocationWithPermission()

        }
        getLocationWithPermission()

    }

    fun getData(lat:String,lng:String){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        if (user.loginStatus!!) {
            Client.getClient()?.create(Service::class.java)
                ?.Favourites(user.userData.id!!, lang.appLanguage, lat, lng)?.enqueue(object :
                Callback<FavouritesResponse> {
                override fun onFailure(call: Call<FavouritesResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                }

                override fun onResponse(
                    call: Call<FavouritesResponse>,
                    response: Response<FavouritesResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                            } else {

//
                                favouritesAdapter.updateAll(response.body()!!.data!!)
                            }
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

            })
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.heart){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.AddFav(lang.appLanguage,user.userData.id!!,favouritesModels.get(position).id!!)?.enqueue(object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext!!,t)
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                           getLocationWithPermission()
                        }else{
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }
                    }
                }

            })
        }else{
            val intent = Intent(activity, ProductDetailsActivity::class.java)
            intent.putExtra("id",favouritesModels.get(position).id)
            startActivity(intent)
        }

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(activity, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
}