package com.aait.taketable.UI.Activities.AppInfo

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.TermsResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Main.MainActivity
import com.aait.taketable.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var about: TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about = findViewById(R.id.about)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish() }
        title.text = getString(R.string.about_app)
        getData()


    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(lang.appLanguage)?.enqueue(object:
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}