package com.aait.taketable.UI.Controllers

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.taketable.R
import com.aait.taketable.UI.Fragments.CurrentFragment
import com.aait.taketable.UI.Fragments.FinishedFragment

class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentFragment()
        } else {
            FinishedFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.current_orders)
        } else {
            context.getString(R.string.finished_orders)
        }
    }
}
