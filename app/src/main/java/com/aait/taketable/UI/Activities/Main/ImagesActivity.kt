package com.aait.taketable.UI.Activities.Main

import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.ImageModel
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.ImagesAdapter

class ImagesActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_images
     lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var images:RecyclerView
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var imagesAdapter: ImagesAdapter
    var imagesModels = ArrayList<ImageModel>()
    override fun initializeComponents() {
        imagesModels = intent.getSerializableExtra("images") as ArrayList<ImageModel>
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        images = findViewById(R.id.images)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.gallery)
        gridLayoutManager = GridLayoutManager(mContext,4)
        imagesAdapter = ImagesAdapter(mContext,ArrayList<ImageModel>(),R.layout.recycle_images)
        images.layoutManager = gridLayoutManager
        images.adapter = imagesAdapter
        imagesAdapter.updateAll(imagesModels)

    }
}