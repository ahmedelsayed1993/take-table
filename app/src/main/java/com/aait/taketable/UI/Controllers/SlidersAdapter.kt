package com.aait.taketable.UI.Controllers

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.aait.taketable.Models.ImageModel
import com.aait.taketable.R
import com.bumptech.glide.Glide
import com.github.islamkhsh.CardSliderAdapter
import kotlinx.android.synthetic.main.card_image_slider.view.*

class SlidersAdapter (context: Context, list : ArrayList<ImageModel>) : CardSliderAdapter<ImageModel>(list) {


    var list = list
    var context=context

    lateinit var image: ImageView
    override fun bindView(position: Int, itemContentView: View, item: ImageModel?) {
        image = itemContentView.image
        Glide.with(context).load(item!!.image).into(image)
        itemContentView.setOnClickListener {
//                val intent  = Intent(context, ImagesActivity::class.java)
//                intent.putExtra("link",list)
//                context.startActivity(intent)
        }


    }


    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }

}