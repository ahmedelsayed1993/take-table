package com.aait.taketable.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.SwitchCompat
import com.aait.taketable.Base.BaseFragment
import com.aait.taketable.Models.BaseResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.AppInfo.AboutAppActivity
import com.aait.taketable.UI.Activities.AppInfo.AppPolicyActivity
import com.aait.taketable.UI.Activities.AppInfo.CallUsActivity
import com.aait.taketable.UI.Activities.AppInfo.LangActivity
import com.aait.taketable.UI.Activities.Auth.LoginActivity
import com.aait.taketable.UI.Activities.Auth.MyDataActivity
import com.aait.taketable.UI.Activities.SplashActivity
import com.aait.taketable.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoreFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_more
    companion object {
        fun newInstance(): MoreFragment {
            val args = Bundle()
            val fragment = MoreFragment()
            fragment.setArguments(args)
            return fragment
        }
    }

    lateinit var profile: LinearLayout

    lateinit var language: LinearLayout
    lateinit var call_us: LinearLayout
    lateinit var policy: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var logout: LinearLayout
    override fun initializeComponents(view: View) {

        profile = view.findViewById(R.id.profile)
        language = view.findViewById(R.id.language)
        call_us = view.findViewById(R.id.call_us)
        policy = view.findViewById(R.id.policy)
        about_app = view.findViewById(R.id.about_app)
        logout = view.findViewById(R.id.logout)

        profile.setOnClickListener {
            if (user.loginStatus!!) {
                startActivity(Intent(activity, MyDataActivity::class.java))
            }else{
                startActivity(Intent(activity,LoginActivity::class.java))
            }
        }
        about_app.setOnClickListener { startActivity(Intent(activity, AboutAppActivity::class.java)) }
        policy.setOnClickListener { startActivity(Intent(activity, AppPolicyActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(activity, CallUsActivity::class.java))  }
        language.setOnClickListener { startActivity(Intent(activity,LangActivity::class.java)) }
        logout.setOnClickListener {
            if (user.loginStatus!!){
                showProgressDialog(getString(R.string.please_wait))
                Client.getClient()?.create(Service::class.java)?.Logout(user.userData.id!!)?.enqueue(object :
                    Callback<BaseResponse>{
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        hideProgressDialog()
                        CommonUtil.handleException(mContext!!,t)
                        t.printStackTrace()
                    }

                    override fun onResponse(
                        call: Call<BaseResponse>,
                        response: Response<BaseResponse>
                    ) {
                        hideProgressDialog()
                        if (response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                user.loginStatus=false
                                user.Logout()
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                                startActivity(Intent(activity, SplashActivity::class.java))
                                activity?.finish()
                            }else{
                                CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                            }
                        }
                    }

                })
            }else{
                startActivity(Intent(activity,LoginActivity::class.java))
                activity!!.finish()
            }
        }


    }
}