package com.aait.taketable.UI.Activities.Main

import android.content.Intent
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.BaseResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_order
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var additions:EditText
    lateinit var book:TextView
    var id =0
    var time = ""
    var date = ""
    var count = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        time = intent.getStringExtra("time")
        date = intent.getStringExtra("date")
        count = intent.getStringExtra("count")
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        additions = findViewById(R.id.additions)
        book = findViewById(R.id.book)
        title.text = getString(R.string.Appointment_Booking)
        back.setOnClickListener { onBackPressed()
        finish()}
        book.setOnClickListener {
            if (CommonUtil.checkEditError(name,getString(R.string.full_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)){
                return@setOnClickListener
            }else{
                Reserve()
            }
        }
    }

    fun Reserve(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.MakeReservation(lang.appLanguage,user.userData.id!!,id,date,time,count,name.text.toString(),user.userData.email!!,phone.text.toString(),1,additions.text.toString())
            ?.enqueue(object : Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            startActivity(Intent(this@OrderActivity,DoneActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}