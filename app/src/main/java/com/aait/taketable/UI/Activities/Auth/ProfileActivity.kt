package com.aait.taketable.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.UserResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Main.MainActivity
import com.aait.taketable.Utils.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.microedition.khronos.egl.EGLDisplay

class ProfileActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var save:Button

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        save = findViewById(R.id.save)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.my_profile_info)
        getData()
        save.setOnClickListener {  if(CommonUtil.checkEditError(name,getString(R.string.user_name))||
            CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
            CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
            return@setOnClickListener
        }else{
            Update()
        }
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Edit(user.userData.id!!,lang.appLanguage,null,null,null)
            ?.enqueue(object: Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            user.userData = response.body()?.data!!
                           // Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                            name.setText( response.body()?.data?.name)
                           // user_name.text = response.body()?.data?.name
                            phone.setText(response.body()?.data?.phone)
                            email.setText(response.body()?.data?.email)

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun Update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Edit(user.userData.id!!,lang.appLanguage,name.text.toString(),phone.text.toString(),email.text.toString())
            ?.enqueue(object: Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            user.userData = response.body()?.data!!
                            // Glide.with(mContext).asBitmap().load(response.body()?.data?.avatar).into(image)
                            name.setText( response.body()?.data?.name)
                            // user_name.text = response.body()?.data?.name
                            phone.setText(response.body()?.data?.phone)
                            email.setText(response.body()?.data?.email)
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}