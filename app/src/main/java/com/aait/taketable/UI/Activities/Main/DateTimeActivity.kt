package com.aait.taketable.UI.Activities.Main

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Listeners.OnItemClickListener
import com.aait.taketable.Models.TimeModel
import com.aait.taketable.Models.TimesResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.TimesAdapter
import com.aait.taketable.Utils.CommonUtil
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.CommonStatusCodes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class DateTimeActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_date_time
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var date:TextView
    lateinit var times:RecyclerView
    lateinit var num:EditText
    lateinit var book:Button
    var id = 0
   var day = ""
    var mYear = 0
    var mMonth = 0
    var mDay = 0
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var timesAdapter: TimesAdapter
    var time = ArrayList<TimeModel>()
    var timing = ""
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        date = findViewById(R.id.date)
        times = findViewById(R.id.times)
        num = findViewById(R.id.num)
        book = findViewById(R.id.book)
        title.text  = getString(R.string.Appointment_Booking)
        back.setOnClickListener { onBackPressed()
        finish()}
        gridLayoutManager  = GridLayoutManager(mContext,4)
        timesAdapter = TimesAdapter(mContext,time,R.layout.recycle_time)
        timesAdapter.setOnItemClickListener(this)
        times.layoutManager = gridLayoutManager
        times.adapter = timesAdapter
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy")
        val formatter1 = DateTimeFormatter.ofPattern("EEEE , dd  MMMM")
        val formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        var answer: String =  current.format(formatter2)
        Log.e("date",answer)
        day = answer
       date.text = day
        times()

        date.setOnClickListener {
            val c = Calendar.getInstance()

            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)

            // Launch Date Picker Dialog
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    date.setText(
                        dayOfMonth.toString() + "/"
                                + (monthOfYear + 1) + "/" + year
                    )
                    times()
                }, mYear, mMonth, mDay)
            dpd.datePicker.minDate = c.timeInMillis
            dpd.show()  }
        book.setOnClickListener { if (CommonUtil.checkTextError(date,getString(R.string.booking_date))||
                CommonUtil.checkEditError(num,getString(R.string.num_of_persons))){
            return@setOnClickListener
        }else{
            val intent = Intent(this,OrderActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("date",date.text.toString())
            intent.putExtra("time",timing)
            intent.putExtra("count",num.text.toString())
            startActivity(intent)
            finish()
        }
        }


    }

    fun times(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Times(lang.appLanguage,user.userData.id!!,id,date.text.toString())?.enqueue(object :
            Callback<TimesResponse> {
            override fun onFailure(call: Call<TimesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TimesResponse>, response: Response<TimesResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        timesAdapter.updateAll(response.body()?.data?.timing!!)
                        timing = response.body()?.data?.timing?.get(0)?.send!!
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.name) {

                timesAdapter.selected = position
                time.get(position).selected = true
                timesAdapter.notifyDataSetChanged()
                timing = time.get(position)?.send!!
                Log.e("time", timing)

        }
    }
}