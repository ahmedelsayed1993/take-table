package com.aait.taketable.UI.Activities.Main

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.NotificationModel
import com.aait.taketable.Models.NotificationResponse
import com.aait.taketable.Models.ProductModel
import com.aait.taketable.Models.ProductsResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.NotificationAdapter
import com.aait.taketable.UI.Controllers.ProductAdapter
import com.aait.taketable.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_notification
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager


    lateinit var notificationAdapter: NotificationAdapter
    var notificationModels = ArrayList<NotificationModel>()

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        back.setOnClickListener { onBackPressed()
            finish()}
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        title.text = getString(R.string.notification)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        notificationAdapter = NotificationAdapter(mContext,notificationModels,R.layout.recycle_notification)
        //  productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = notificationAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
        back.setOnClickListener { onBackPressed()
        finish()}
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
//        subs.clear()
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notification(lang.appLanguage,user.userData.id!!)?.enqueue(object:
            Callback<NotificationResponse> {
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<NotificationResponse>,
                response: Response<NotificationResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {

//
                            notificationAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}