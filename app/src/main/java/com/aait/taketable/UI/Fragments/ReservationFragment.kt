package com.aait.taketable.UI.Fragments

import android.os.Bundle
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.taketable.Base.BaseFragment
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class ReservationFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_reservations
    companion object {
        fun newInstance(): ReservationFragment {
            val args = Bundle()
            val fragment = ReservationFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }
}