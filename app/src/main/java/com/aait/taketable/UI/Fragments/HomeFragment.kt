package com.aait.taketable.UI.Fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.taketable.Base.BaseFragment
import com.aait.taketable.GPS.GPSTracker
import com.aait.taketable.GPS.GpsTrakerListener
import com.aait.taketable.Listeners.OnItemClickListener
import com.aait.taketable.Models.HomeResponse
import com.aait.taketable.Models.ProviderModel
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Main.ProductDetailsActivity
import com.aait.taketable.UI.Activities.Main.SearchActivity
import com.aait.taketable.UI.Controllers.ProvidersAdapter
import com.aait.taketable.UI.Controllers.RecommendedAdapter
import com.aait.taketable.UI.Controllers.SliderAdapter
import com.aait.taketable.Utils.CommonUtil
import com.aait.taketable.Utils.DialogUtil
import com.aait.taketable.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment:BaseFragment() ,OnItemClickListener, GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }

    lateinit var viewPager:CardSliderViewPager
    lateinit var search:TextView
    lateinit var show:TextView
    lateinit var restaurants:RecyclerView
    lateinit var show_al:TextView
    lateinit var cafes:RecyclerView
    lateinit var show_all:TextView
    lateinit var recommended:RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var providersAdapter: ProvidersAdapter
    lateinit var providersAdapter1: ProvidersAdapter
    lateinit var recommendedAdapter: RecommendedAdapter
    var providerModels = ArrayList<ProviderModel>()
    var providerModels1 = ArrayList<ProviderModel>()
    var recommendedModels = ArrayList<ProviderModel>()
    internal  var googleMap: GoogleMap?=null
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    var rest = 0
    var caf = 0
    private var mAlertDialog: AlertDialog? = null
    override fun initializeComponents(view: View) {
        viewPager = view.findViewById(R.id.viewPager)
        search = view.findViewById(R.id.search)
        show = view.findViewById(R.id.show)
        restaurants = view.findViewById(R.id.restaurants)
        show_al = view.findViewById(R.id.show_al)
        cafes = view.findViewById(R.id.cafes)
        show_all = view.findViewById(R.id.show_all)
        recommended = view.findViewById(R.id.recommended)
        linearLayoutManager = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        linearLayoutManager2 = LinearLayoutManager(mContext!!,LinearLayoutManager.HORIZONTAL,false)
        providersAdapter = ProvidersAdapter(mContext!!,providerModels,R.layout.recycle_provider)
        providersAdapter1 = ProvidersAdapter(mContext!!,providerModels,R.layout.recycle_provider)
        recommendedAdapter = RecommendedAdapter(mContext!!,providerModels,R.layout.recycle_recommended)
        providersAdapter.setOnItemClickListener(this)
        providersAdapter1.setOnItemClickListener(this)
        recommendedAdapter.setOnItemClickListener(this)
        restaurants.layoutManager = linearLayoutManager
        cafes.layoutManager = linearLayoutManager1
        recommended.layoutManager = linearLayoutManager2
        restaurants.adapter = providersAdapter
        cafes.adapter = providersAdapter1
        recommended.adapter = recommendedAdapter
        getLocationWithPermission()
        show.setOnClickListener { val intent = Intent(activity,SearchActivity::class.java)
        intent.putExtra("cat",rest)
        startActivity(intent)}
        show_al.setOnClickListener { val intent = Intent(activity,SearchActivity::class.java)
            intent.putExtra("cat",caf)
            startActivity(intent)}
        show_all.setOnClickListener { val intent = Intent(activity,SearchActivity::class.java)
            intent.putExtra("cat",0)
            startActivity(intent)}
        search.setOnClickListener { val intent = Intent(activity,SearchActivity::class.java)
            intent.putExtra("cat",0)
            startActivity(intent)}
    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.getHome(lang.appLanguage,gps.latitude.toString(),gps.longitude.toString())?.enqueue(object :Callback<HomeResponse>{
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        rest = response.body()?.data?.category?.get(0)?.id!!
                        caf = response.body()?.data?.category?.get(1)?.id!!
                        providersAdapter.updateAll(response.body()?.data?.category?.get(0)?.providers!!)
                        providersAdapter1.updateAll(response.body()?.data?.category?.get(1)?.providers!!)
                        recommendedAdapter.updateAll(response.body()?.data?.providers!!)
                        initSliderAds(response.body()?.data?.slider!!)
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity,ProductDetailsActivity::class.java)
        intent.putExtra("id",providerModels.get(position).id)
        startActivity(intent)

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getData(lat:String,lng:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getHome(lang.appLanguage,lat,lng)?.enqueue(object :Callback<HomeResponse>{
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        rest = response.body()?.data?.category?.get(0)?.id!!
                        caf = response.body()?.data?.category?.get(1)?.id!!
                        providersAdapter.updateAll(response.body()?.data?.category?.get(0)?.providers!!)
                        providersAdapter1.updateAll(response.body()?.data?.category?.get(1)?.providers!!)
                        recommendedAdapter.updateAll(response.body()?.data?.providers!!)
                        initSliderAds(response.body()?.data?.slider!!)
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun initSliderAds(list: java.util.ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility=View.GONE
        }
        else{
            viewPager.visibility=View.VISIBLE
            viewPager.adapter= SliderAdapter(mContext!!,list)
        }
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(activity, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())
                getData(gps.latitude.toString(),gps.longitude.toString())

            }
        }
    }
}