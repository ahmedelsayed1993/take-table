package com.aait.taketable.UI.Activities
import android.content.Intent
import android.os.Handler
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.R
import com.aait.taketable.UI.Activities.Auth.LoginActivity
import com.aait.taketable.UI.Activities.Main.MainActivity
class SplashActivity : ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false
    override fun initializeComponents() {
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){
                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    this@SplashActivity.finish()
                }else {
                    var intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }, 2100)
        }, 1800)
    }
}
