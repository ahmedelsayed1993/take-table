package com.aait.taketable.UI.Activities.Main

import android.Manifest
import android.content.ClipData
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.GPS.GPSTracker
import com.aait.taketable.GPS.GpsTrakerListener
import com.aait.taketable.Models.BaseResponse
import com.aait.taketable.Models.ImageModel
import com.aait.taketable.Models.ReservationModel
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.UI.Controllers.SlidersAdapter
import com.aait.taketable.Utils.CommonUtil
import com.aait.taketable.Utils.DialogUtil
import com.aait.taketable.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class ReservationDetailsActivity:ParentActivity(), OnMapReadyCallback,
    GpsTrakerListener {
    override val layoutResource: Int
        get() = R.layout.activity_reservation_details
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var rating: RatingBar
    lateinit var name:TextView
    lateinit var type:TextView
    lateinit var distance:TextView
    lateinit var date:TextView
    lateinit var time:TextView
    lateinit var count:TextView
    lateinit var map:MapView
    lateinit var go_now:Button
    lateinit var cancel:Button
    private var mAlertDialog: AlertDialog? = null
    internal  var googleMap: GoogleMap?=null
    internal  var myMarker: Marker?=null
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    lateinit var reservationModel: ReservationModel
    override fun initializeComponents() {
        reservationModel = intent.getSerializableExtra("data") as ReservationModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        rating = findViewById(R.id.rating)
        name = findViewById(R.id.name)
        type = findViewById(R.id.type)
        distance = findViewById(R.id.distance)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        count = findViewById(R.id.num)
        map = findViewById(R.id.map)
        go_now = findViewById(R.id.go_now)
        cancel = findViewById(R.id.cancel)
        title.text = getString(R.string.reservation_details)
        back.setOnClickListener { onBackPressed()
        finish()}
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        getLocationWithPermission()
        Glide.with(mContext).load(reservationModel.image).into(image)
        rating.rating = reservationModel.rate!!
        name.text = reservationModel.name
        type.text = reservationModel.food_type
        distance.text = getString(R.string.Away_from_you)+reservationModel.distance+getString(R.string.km)
        date.text = reservationModel.date
        time.text = reservationModel.time
        count.text = reservationModel.numbers.toString()
        if (reservationModel.status.equals("pending")||reservationModel.status.equals("accepted")){
            cancel.visibility = View.VISIBLE
        }else{
            cancel.visibility = View.GONE
        }
        go_now.setOnClickListener {
            Log.e("ttt","rrr")
           startActivity( Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + gps.latitude.toString() + "," + gps.longitude.toString() + "&daddr=" + reservationModel.lat + "," + reservationModel.lng)
            ))
        }
        cancel.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Cancel(lang.appLanguage,reservationModel.id!!)?.enqueue(object :
                Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            startActivity(Intent(this@ReservationDetailsActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }
    }

//    override fun onMapReady(p0: GoogleMap?) {
//        this.googleMap = p0!!
//        getLocationWithPermission()
//        hideProgressDialog()
//    }
    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap?.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.outline_location_on))
        )!!
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }


    fun getLocationWithPermission() {
        gps = GPSTracker(mContext!!, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext!!,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        }
        else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }


                //putMapMarker(gps.getLatitude(), gps.getLongitude())


            }
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0
        getLocationWithPermission()
    }
}