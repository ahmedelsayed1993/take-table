package com.aait.taketable.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.aait.taketable.Base.ParentActivity
import com.aait.taketable.Models.UserResponse
import com.aait.taketable.Network.Client
import com.aait.taketable.Network.Service
import com.aait.taketable.R
import com.aait.taketable.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var user_name: EditText
    lateinit var phone: EditText
    lateinit var email:EditText
    lateinit var password: EditText
    lateinit var confirm_password: EditText
    lateinit var login: LinearLayout
    lateinit var register: Button
    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_pass)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        email = findViewById(R.id.email)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        register.setOnClickListener {
            if(CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(email,getString(R.string.email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())) {
                    CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                }else{
                    Register()
                }
            }
        }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp("client",user_name.text.toString(),phone.text.toString(),email.text.toString()
            ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object:
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}